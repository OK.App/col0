#@tool
class_name ChunkExplorer
extends Node3D


@export_category("Chunk") ###############

@onready var _chunk			: Chunk = %Chunk

@export
## press [param update] to have the [param chunk] recreated
var update			: bool = false: 
	set(new_start):
		print ("Set update ", new_start)
		update = new_start
		if update:
			updateChunk()
		else:
			pass #delete_terrain_mesh()
		
		update = false

## chunk size, chunk will be subdivided
@export var chunk_size		: int = 32:
	get:
		return 0 if !_chunk else _chunk.size
		
	set(value):
		if !_chunk:
			return
		_chunk.size = value
		updateChunk()

@export var chunk_divides	: int = 200:
	get:
		return 0 if !_chunk else _chunk.divides
	
	set(value):
		if !_chunk:
			return
		_chunk.divides = value
		updateChunk()

@export var chunk_amplitude : float = 100:
	get:
		return .0 if !_chunk else _chunk.amplitude
	
	set(value):
		if !_chunk:
			return
		_chunk.amplitude = value
		updateChunk()

@export var chunk_elevation : float = 0:
	get:
		return .0 if !_chunk else _chunk.elevation
	
	set(value):
		if !_chunk:
			return
		_chunk.elevation = value
		updateChunk()

@export var chunk_easing 	: float = 1:
	get:
		return .0 if !_chunk else _chunk.easing
	
	set(value):
		if !_chunk:
			return
		_chunk.easing = value
		updateChunk()
		

func _ready():
	print ("\nReady for: ", _chunk)
	updateChunk()


func updateChunk():
	print ("Updating chunk", _chunk, ", has method 'generate_terrain_mesh': ", _chunk.has_method("generate_terrain_mesh"))
	if _chunk.has_method("generate_terrain_mesh"):
		_chunkgenerate_terrain_mesh()

