@tool
class_name Chunk
extends Node3D

@export var update: bool = false: 
	set(_new_start):
		generate_terrain_mesh()

@export var live: bool = false:
	set(v):
		live = v
		if live:
			generate_terrain_mesh()

@export var size		: int = 32:
	set(v):
		if size != v:
			size = v
			if live:
				generate_terrain_mesh()

@export var divides		: int = 16:
	set(v):
		if divides != v:
			divides = v
			if live:
				generate_terrain_mesh()

@export var lods: PackedInt32Array = [1, 2, 4, 8, 16, 32]

@export_range(0, 6) var lod	: int = 1:
	set(v):
		if lod != v:
			lod = v
			if live:
				generate_terrain_mesh()

@export var elevation 	: float = 0:
	set(v):
		if elevation != v:
			elevation = v
			if live:
				generate_terrain_mesh()

@export var amplitude	: float = 500:
	set(v):
		if amplitude != v:
			amplitude = v
			if live:
				generate_terrain_mesh()

@export var easing		: float = 1:
	set(v):
		if easing != v:
			easing = v
			if live:
				generate_terrain_mesh()

@export var map_offset  : Vector3 = Vector3.ZERO:
	set(v):
		if map_offset != v:
			map_offset = v
			if live:
				generate_terrain_mesh()

@export var height_map	: Noise:
	set(v):
		if height_map != v:
			height_map = v
			if live:
				generate_terrain_mesh()

@export var terrain_material: ShaderMaterial = preload("res://Materials/terrain_material.tres"):
	set(v):
		if terrain_material != v:
			terrain_material = v
			if live:
				generate_terrain_mesh()

@export var water_material: Material = preload("res://Materials/water.material"):
	set(v):
		if water_material != v:
			water_material = v
			if live:
				generate_terrain_mesh()


var mesh_instance: MeshInstance3D = null
var surface_tool := SurfaceTool.new()
var data_tool 	 := MeshDataTool.new()

var water_mesh_instance: MeshInstance3D

var height_easing: VisualShaderNodeFloatFunc

var biomes: Biomes = null


func _init (heightMap: Noise = FastNoiseLite.new(), 
			world_pos: Vector3 = Vector3.ZERO, 
			chunk_size: int = 32,
			chunk_lod: int = 0,
			amplitude_for_noise: float = 50, 
			chunk_biome: Biomes = null):
	
	print ("Creating chunk at ", world_pos, " ", chunk_size, "x", chunk_size, ", amplitude: ", amplitude_for_noise)

	self.map_offset = Vector3(world_pos.x, 0, world_pos.z)
	self.height_map = heightMap
	self.size = chunk_size
	self.lod = chunk_lod
	self.divides = (chunk_size / lods[lod])
	self.amplitude = amplitude_for_noise
	self.elevation = world_pos.y
	self.biomes = chunk_biome


# Called when the node enters the scene tree for the first time.
func _ready():
	if biomes:
		self.amplitude = biomes.amplitude
	
	generate_terrain_mesh()
	generate_water()


func ti(start_ticks) -> int:
	return Time.get_ticks_msec() - start_ticks


func ease_height (linear_input: float) -> float:
	#return ease(linear_input, self.easing)
	#return smoothstep(-1.0, 1-0, linear_input)
	return linear_input * linear_input * sign(linear_input)


func generate_terrain_mesh():
	const benchmark := false
	var st = Time.get_ticks_msec()

	self.divides = (self.size / self.lods[self.lod])
	print("Generating terrain, chunk size: ", self.size, ", divides: ", self.divides)
	
	# Delete previous mesh first, if there is any
	if benchmark:
		print (ti(st), "  Deleting previous mesh")
	
	delete_terrain_mesh()

	# Generate plane mesh
	if benchmark:
		print (ti(st), "  Creating Plane Mesh, size: ", self.size, ", divides: ", self.divides)
	
	var plane_mesh := PlaneMesh.new()
	plane_mesh.size 			= Vector2i(self.size, self.size)
	plane_mesh.subdivide_depth 	= self.divides
	plane_mesh.subdivide_width 	= self.divides
	
	# Pass it to surface tool
	if benchmark:
		print (ti(st), "  Creating surface tool from plane")
	
	surface_tool.create_from(plane_mesh, 0)
	
	# Create array mesh using surface tool
	if benchmark:
		print (ti(st), "  Creating array mesh from surface")
	
	var array_mesh: ArrayMesh = surface_tool.commit()
	
	# Pass array mesh to data tool
	if benchmark:
		print (ti(st), "  Creating data tool from array mesh")
	
	data_tool.create_from_surface(array_mesh, 0)
	
	# use data tool to raise all vertices by height from height_map and amplitude
	# while adding general chunk elevation
	if benchmark:
		print (ti(st), "  Adjusting height for ", data_tool.get_vertex_count(), " vertices")
	
	for i in range(data_tool.get_vertex_count()):
		var vertex := data_tool.get_vertex(i)
		var map_pos := vertex + self.map_offset
		var height = (biomes.heightAt (map_pos) 
						if biomes 
						else ease_height (height_map.get_noise_2d(map_pos.x, map_pos.z)) * amplitude)
		
		if height > amplitude:
			print ("height too large: ", height)
			
		vertex.y = self.elevation + height
		data_tool.set_vertex(i, vertex)
	
	#remove flat surfaces from array mesh
	if benchmark:
		print (ti(st), "  Clearing previous mesh")
	
	array_mesh.clear_surfaces()
	
	# Create mesh data: triangles and normals
	if benchmark:
		print (ti(st), "  Committing vertex data from data tool to surface tool")
	
	data_tool.commit_to_surface(array_mesh)
	surface_tool.begin(Mesh.PRIMITIVE_TRIANGLES)
	surface_tool.create_from(array_mesh, 0)
	surface_tool.generate_normals()
	
	# Create MeshInstamce from triangles and normals, add material and collision
	if benchmark:
		print (ti(st), "  Creating mesh instance from surface tool")
	
	mesh_instance = MeshInstance3D.new()
	mesh_instance.mesh = surface_tool.commit()
	mesh_instance.set_surface_override_material(0, terrain_material)
	terrain_material.set_shader_parameter("amplitude", self.amplitude)	#self.elevation +
	mesh_instance.create_trimesh_collision();
	
	if false && biomes:
		var shaderMaterial: ShaderMaterial = mesh_instance.get_surface_override_material(0) 
		var biome:  Biome = biomes.biomeAt(self.map_offset)
		shaderMaterial.set_shader_parameter("_biomeTint", biome.tint)
		
	# add to scene tree
	add_child(mesh_instance)
	print (ti(st), "  Chunk mesh done")


func delete_terrain_mesh():
	if mesh_instance:
		print ("Deleting old terrain...")
		mesh_instance.queue_free()
		mesh_instance = null


func generate_water():
	var plane_mesh := PlaneMesh.new()
	plane_mesh.size = Vector2i(self.size, self.size)
	#plane_mesh.center_offset = Vector3(0, self.elevation - self.amplitude * 0.4, 0)
	##var biome_elevation = self.elevation #biome.elevationAt (self.position)
	#plane_mesh.center_offset = Vector3(0, biome_elevation, 0)
	plane_mesh.material = water_material
	water_mesh_instance = MeshInstance3D.new()
	water_mesh_instance.mesh = plane_mesh
	add_child(water_mesh_instance)
	
# blending two textures with height: https://youtu.be/_Yv5xWvIJ_w?si=_KdiyJJyyCugsQQz

