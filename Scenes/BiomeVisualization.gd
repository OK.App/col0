extends GridContainer

@onready var biomes: Biomes = $"../.."

var biome_map: Array [Array]
var color_rects: Array [Array]
var rect_width: float
var rect_height: float
var num_rows := Biomes.HUMID_LEVELS
var num_columns := Biomes.TEMP_LEVELS

# Called when the node enters the scene tree for the first time.
func _ready():
	rect_width = self.scale.x / num_rows
	rect_height = self.scale.y / num_columns

	color_rects.resize(num_rows)
	for row in num_rows:
		color_rects[row].resize(num_columns)
		for column in num_columns:
			#print("Create color rect ", row, "/",column)
			var color_rect = ColorRect.new()
			color_rect.set_position(Vector2(1.0 * row * rect_width, 1.0 * column * rect_height))
			add_child(color_rect)
			color_rects[row][column] = color_rect


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	if biome_map != biomes.humTempMap:
		biome_map = biomes.humTempMap
		create_color_grid()


func create_color_grid():
	print ("Creating color grid...")
	for column in biome_map.size():
		for row in biome_map.size():
			var biome = biome_map[row][column]
			var color_rect = color_rects[row][column]
			color_rect.color = biome.tint if biome and biome is Biome else Color.BLUE_VIOLET
