class_name Player
extends CharacterBody3D


@export var speed := 50.0
@export var mouse_sensitivity := 0.08

@onready var playerCam = %MainCamera


signal positionChanged(position)


func _ready() -> void:
	# hide mouse pointer
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)


func _input(event) -> void:
	aim(event)


func _physics_process(delta) -> void:
	move(delta)


func aim(event: InputEvent) -> void:
	# use mouse to pan and tilt the camera
	var mouseMovedEvent = event as InputEventMouseMotion
	if mouseMovedEvent:
		# rotate around vertical axis by horizontal mouse movement 
		rotation_degrees.y -= mouseMovedEvent.relative.x * mouse_sensitivity
	
		# rotate camera around x axis by vertical mouse movement
		var tilt = playerCam.rotation_degrees.x - mouseMovedEvent.relative.y * mouse_sensitivity
		playerCam.rotation_degrees.x = clamp (tilt, -90, 90)


func move(_delta) -> void:

	var foreback  = Input.get_axis("moveForward", "moveBack") * transform.basis.z
	var leftright = Input.get_axis("moveLeft", "moveRight") * transform.basis.x
	var upDown    = Input.get_axis("moveDown", "moveUp") * transform.basis.y

	var direction = (foreback + leftright + upDown).normalized()
	
	velocity = direction * speed
	
	move_and_slide()	
	
