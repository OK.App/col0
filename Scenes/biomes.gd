@tool
class_name Biomes
extends Node2D

const HUMID_LEVELS := 10
const TEMP_LEVELS := 10

@export var amplitude: float = 200.0

var humTempMap: Array[Array] = [
	[ 9,9,9,9,9,9,9,9,9,9],
	[ 9,9,9,9,9,9,9,9,9,9],
	[ 9,9,9,9,9,9,9,9,9,9],
	[ 9,9,9,9,9,9,9,9,9,9],
	[ 9,9,9,9,9,9,9,9,9,9],
	[ 9,9,9,9,9,9,9,9,9,9],
	[ 9,9,9,9,9,9,9,9,9,9],
	[ 9,9,9,9,9,9,9,9,9,9],
	[ 9,9,9,9,9,9,9,9,9,9],
	[ 9,9,9,9,9,9,9,9,9,9],
] # will be 10x10

enum NoiseMap {
	Humidity, Temperature, Elevation, Height, NumNoiseMaps #TODO: ores, vegetation
}


## These are the noise maps for humidity, temperature and elevation.
@export var noiseMaps			: Array[FastNoiseLite]
@export var height_ease			: float = 1.0
@export var elevation_weight	: float = 0.3


func setup_noiseMaps():
	print ("Creating Biome class, setting up ", NoiseMap.NumNoiseMaps, " noise maps")
	for n in range (0, NoiseMap.NumNoiseMaps):
		# if noisemap has been created in inspector, use it
		if (noiseMaps[n]):
			print ("Using custom noise ", n)
			continue
		
		# create a noisemap using standard values as default
		print ("Creating default noise", n)
		var noiseMap = FastNoiseLite.new()
		noiseMap.frequency = .002
		noiseMap.fractal_octaves = 2
		noiseMap.noise_type = FastNoiseLite.TYPE_VALUE
		noiseMaps.append(noiseMap)


func setup_biomeMap():
	print ("Setting up humidity/temperature to Biome type map")
	for child in get_children():
		if not child is Biome:
			continue
		
		var biome := child as Biome
		
		print (biome.name, ": ", biome.minHumidity, "-", biome.maxHumidity, ", ", biome.minTemperature, "-", biome.maxTemperature)	
		for h in range (biome.minHumidity, biome.maxHumidity+1):
			for t in range (biome.minTemperature, biome.maxTemperature+1):
				#print ("Setting ", h, "/", t, " to ", biome.name)
				humTempMap[h][t] = biome

	# Output map
	for h in range(0, HUMID_LEVELS):
		var row := ""
		for t in range(0, TEMP_LEVELS):
			var biome = humTempMap[h][t]
			row += biome.name + ' ' if biome is Biome else "******** "
		
		print (row)


func _ready():
	setup_noiseMaps()
	setup_biomeMap()


func getNoise (n: NoiseMap, pos: Vector3) -> float:
	return noiseMaps[n].get_noise_2d(pos.x, pos.z)


func elevationAt (pos: Vector3) -> float:
	return getNoise(NoiseMap.Elevation, pos)  * elevation_weight


func unelevatedHeightAt (pos: Vector3) -> float:
	var base_height = getNoise(NoiseMap.Height, pos) * (1.0 - elevation_weight)
	#var eased_height = ease (base_height, height_ease)
	return base_height


func heightAt (pos: Vector3) -> float:
	var eased_height = unelevatedHeightAt(pos)
	var elevated_height = ease (eased_height + elevationAt(pos), height_ease)
	var amplituded_height = elevated_height * amplitude
	
	return amplituded_height

	
func biomeAt (pos: Vector3) -> Biome:
	var humidity: int    = floori(getNoise(NoiseMap.Humidity, pos) * HUMID_LEVELS)
	var temperature: int = floori(getNoise(NoiseMap.Temperature, pos) * TEMP_LEVELS)
	var biome: Biome 	 = humTempMap[humidity][temperature]
	return biome
