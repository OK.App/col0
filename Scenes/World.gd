#@tool
class_name World
extends Node3D


@export_category("Random") ##################

@export var noise_type			: FastNoiseLite.NoiseType = FastNoiseLite.TYPE_VALUE: # FastNoiseLite.TYPE_PERLIN:
	set(value):
		noise_type = value
		generate_terrain_mesh()

@export var noise_frequency		: float = 0.002:
	set(value):
		noise_frequency = value
		generate_terrain_mesh()

@export var noise_octaves		: int 	= 6:
	set(value):
		noise_octaves = value
		generate_terrain_mesh()

@export var grid_size			: int = 32:
	set(value):
		grid_size = value
		generate_terrain_mesh()
		
@export var amplitude			: float = 100:
	set(value):
		amplitude = value
		generate_terrain_mesh()

@export var elevation			: float = 0:
	set(value):
		elevation = value
		generate_terrain_mesh()


@export var chunk_distance: int = 8	# number of chunks to hold currently
var chunk_distance2D = Vector2i(chunk_distance, chunk_distance)

@onready var player: Player = $Player if $Player else null
@onready var biomes: Biomes = $Biomes

var chunks = {}
var chunks_creating = {}
var height_map: FastNoiseLite
var thread := Thread.new()


func test_far(point: Vector2i):
	print ("Far: ", point, ": ", is_far(point, Vector2i(30,30)))	


func _ready():
	print ("Starting world, grid size_ ", grid_size, ", amplitude: ", amplitude, ", elevation: ", elevation)
	biomes.amplitude = self.amplitude
	generate_terrain_mesh()


func generate_terrain_mesh():
	# Generate noise to provide the height map
	height_map = FastNoiseLite.new()
	height_map.noise_type 		= self.noise_type
	height_map.frequency 		= self.noise_frequency
	height_map.fractal_octaves 	= self.noise_octaves
	#reload all chunks


func create_chunk (grid_pos: Vector2i, lod: int):
	if chunks_creating.has(grid_pos):
		return
	
	if chunks.has(grid_pos):
		var chunk = get_chunk(grid_pos)
		if chunk.lod == lod:
			return
		else:
			remove_chunk (grid_pos, chunk)

	print("adding task to create chunk: ", grid_pos)
	chunks_creating[grid_pos] = true
	WorkerThreadPool.add_task(_thread_create_chunk.bind(grid_pos, lod, null),false, "create chunk")


func gridToWorld (grid_pos: Vector2i, y_pos: float) -> Vector3:
	return Vector3(	grid_pos.x * grid_size, 
					y_pos, 
					grid_pos.y * grid_size)


func worldToGrid(world_pos: Vector3) -> Vector2i:
	return Vector2i(	floori(world_pos.x / grid_size), 
						floori(world_pos.z / grid_size))


func _thread_create_chunk(grid_pos: Vector2i, lod: int, _thread: Thread):
	var world_pos: Vector3 = gridToWorld(grid_pos, self.elevation)
	var chunk = Chunk.new(self.height_map, world_pos, self.grid_size, lod, self.amplitude, self.biomes)
	
	# Let main task insert the chunk into the world
	call_deferred("chunk_created", grid_pos, world_pos, chunk)


func chunk_created(grid_pos: Vector2i, world_pos: Vector3, chunk: Chunk):
	print("chunk created:", grid_pos)
	chunks [grid_pos] = chunk
	chunks_creating.erase(grid_pos)
	chunk.position = world_pos
	add_child(chunk)
#	thread.wait_to_finish()


func get_chunk (grid_pos: Vector2i) -> Chunk:
	return (chunks [grid_pos]
		 	if chunks.has(grid_pos) 
			else null)


func get_manhattan_length(grid_pos: Vector2i, center: Vector2i = worldToGrid(player.position)) -> int:
	var manhattanLength: int = absi(grid_pos.x - center.x) + absi(grid_pos.y - center.y)
	return manhattanLength;
	

## Is the grid position given far ayway from a reference position?
## Here "far away" means, farer than the chunk_distance
## Default reference postion is the player's grid_position 
func is_far(grid_pos: Vector2i, center: Vector2i = worldToGrid(player.position)):
	var manhattanLength := get_manhattan_length(grid_pos, center)
	return manhattanLength > chunk_distance + chunk_distance
 

func _physics_process(_delta):
	create_near_chunks()
	remove_far_chunks()


## This function makes sure all chunks within the player's position will be created
func create_near_chunks():
	if (player):
		# load all chunks around player
		var player_pos = player.position
		var grid_pos: Vector2i = worldToGrid(player_pos)

		# Iterate all grid positions around player grid position
		for x in range (grid_pos.x - chunk_distance, grid_pos.x + chunk_distance):
			for y in range(grid_pos.y - chunk_distance, grid_pos.y + chunk_distance):
				var chunk_pos := Vector2i(x,y)
				var lod = clampi(get_manhattan_length(chunk_pos, grid_pos) / 2, 0, 5)
				create_chunk(chunk_pos, lod) #, elevation)


## remove all chunks that are too far away
func remove_far_chunks():
	for key in chunks:
		var chunk: Chunk = get_chunk(key)
		if chunk and is_far(key):
			remove_chunk(key, chunk)


func remove_chunk(key:Vector2i, chunk: Chunk):
	print ("removing chunk ", key)
	chunk.queue_free()
	chunks.erase(key)
	
