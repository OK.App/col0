class_name Biome
extends Node

@onready var biomes: Biomes = $".."

enum Biomes {
	Arctic, Tundra, Grasslands, Forest, Savannah, Jungle, Swamp, Desert, Lava, NumBiomes
}

const HUMID_LEVELS := 10
const TEMP_LEVELS := 10
const MAX_ELEVATION := 200


#@export var name: String = "Biome Name"
@export_enum("-40°C:0", "-30°C:1", "-20°C:2", "-10°C:3", "0°C:4", "10°C:5", "20°C:6", "30°C:7", "40°C:8", ">50°C:9") var minTemperature: int
@export_enum("-40°C:0", "-30°C:1", "-20°C:2", "-10°C:3", "0°C:4", "10°C:5", "20°C:6", "30°C:7", "40°C:8", ">50°C:9") var maxTemperature: int
@export_enum("0%:0", "10%:1", "20%:2", "30%:3", "40%:4", "50%:5", "60%:6", "70%:7", "80-90%:8", "100%:9") var minHumidity: int
@export_enum("0%:0", "10%:1", "20%:2", "30%:3", "40%:4", "50%:5", "60%:6", "70%:7", "80-90%:8", "100%:9") var maxHumidity: int
@export var tint: Color;

#@export

var elevation_amplitude := MAX_ELEVATION

