extends RigidBody3D
class_name Mob


@export var speed := 300.0

var direction = Vector2.ZERO
var targetPos = position


func _physics_process(_delta):

	# Get the input direction and handle the movement/deceleration.
	# As good practice, you should replace UI actions with custom gameplay actions.
	#var way = position - targetPos
	if position.distance_to(targetPos) > 0:
		direction = position.direction_to(targetPos)
	else:
		direction = Vector2.ZERO
	
	#velocity = direction * speed
	#move_and_slide()

func _unhandled_key_input(event):
	if event.is_action("ui_accept"):
		print("mob!")
		direction = Vector2.RIGHT

func walkTo (pos: Vector3):
	self.look_at(pos)
	targetPos = pos
